<?php
	require "database.php";
	require "session_auth.php";
	$username=$_SESSION["username"];
	$usercomment=$_REQUEST["usercomment"];
    $post_id=$_GET['post_id'];

	if (isset($username) AND isset($usercomment) AND isset($post_id)) {
		if (!writecomment($username, $usercomment, $post_id)) {
			echo "<script>alert('The post was not stored in the database');</script>";
		} 
	} else {
        echo "<script>alert('Error gathering the username or post.');</script>";
		exit();
	}
    header("Refresh:0; url=index.php");
?>

