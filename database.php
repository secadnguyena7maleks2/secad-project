<?php
	$mysqli = new mysqli('localhost', 'nguyenmalek', 'mysecretP4$$', 'secadproject');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_error);
		exit();
	}
	function changepassword($username, $newpassword) {
		if (validatePassword($newpassword) == FALSE) {
			return FALSE;
		}
		global $mysqli;
		$prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ss", $newpassword, $username);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function addnewuser($username, $password) {
		if (validateUsername($username) == FALSE or validatePassword($password) == FALSE) {
			return FALSE;
		}
		global $mysqli;
		$prepared_sql = "INSERT INTO users VALUES (?, password(?),1);";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ss", $username, $password);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function edit($post_id, $message, $type) {
		if(empty($message) or strlen($message) < 1 or strlen($message) > 3000) {
			return FALSE;
		}
		global $mysqli;
        $prepared_sql = "UPDATE $type SET message=? WHERE " . $type . "_id= ?;";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("si", $message, $post_id);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function deleteitem($post_id, $type) {
		global $mysqli;
        $prepared_sql = "DELETE FROM $type WHERE " . $type . "_id= ?;";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("i", $post_id);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function changeusersettings($checklist){
		global $mysqli;
		$prepared_sql = "UPDATE users SET enabled=0 WHERE username != 'nguyenmalek';";
    	if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    	if(!$stmt->execute()) return FALSE;

    	foreach ($checklist as $check => $user) {
    		$prepared_sql = "UPDATE users SET enabled=1 WHERE username=?;";
    		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    		$stmt->bind_param('s', $user);
    		if(!$stmt->execute()) return FALSE;
    	}
    	return TRUE;
	}

	function writepost($owner, $message) {
		if(empty($message) or strlen($message) < 1 or strlen($message) > 3000) {
			return FALSE;
		}
		global $mysqli;
        $time_stamp = date('Y-m-d H:i:s');
		$prepared_sql = "INSERT INTO posts(time_stamp, owner, message) VALUES (?, ?, ?);";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("sss", $time_stamp, $owner, $message);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function writecomment($owner, $message, $post_id) {
		if(empty($message) or strlen($message) < 1 or strlen($message) > 3000) {
			return FALSE;
		}
		global $mysqli;
        $time_stamp = date('Y-m-d H:i:s');
		$prepared_sql = "INSERT INTO comments(posts_id, message, time_stamp, owner) VALUES (?, ?, ?, ?);";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("isss", $post_id, $message, $time_stamp, $owner);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function validateUsername($username) {
		$username = sanitize_input($username);
		if(empty($username) or strlen($username) < 6 or strlen($username) > 40) {
			return FALSE;
		}
		return TRUE;
	}

	function validatePassword($password) {
		$password = sanitize_input($password);
		if(empty($password) or strlen($password) < 6 or strlen($password) > 40) {
			return FALSE;
		}
		return TRUE;
	}

	function sanitize_input($input) {
		$input = trim($input);
		$input = stripslashes($input);
		$input = htmlspecialchars($input);
		return $input;
	}
?>
