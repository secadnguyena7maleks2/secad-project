<?php
	require "session_auth.php";
	require "database.php";
	$username=$_SESSION["username"];
	$checkedlist = $_POST["checkedlist"];

	if (isset($username) AND isset($checkedlist)) {
		if (changeusersettings($checkedlist)) {
			echo "<script>alert('The new enable/disable setting has been set.');</script>";
		} else {
			echo "<script>alert('Error: Cannot enable/disable the user');</script>";
		}
	} else {
		echo "<script>alert('You cannot disable every user from the website! Try again');</script>";
	}
	header("Refresh:0; url=index.php");
?>