# CPS 475/575 Secure Application Development Final Project

## Professor Phu Phung


By: Sean Malek (ID#: 1013533190), maleks2@udayton.edu  
    Antall Nguyen (ID#: 101451472), nguyena7@udayton.edu


The code for this project can be seen at this [link](https://bitbucket.org/secadnguyena7maleks2/secad-project/src/master/.).  

The video demonstration can be seen on YouTube [here](https://youtu.be/eNLFZ0IR1LM).  


# 1. Introduction

In this programming project, all secure programming principles and web developent technologies that were learned throughout the semester were applied to develop a simple and secure "miniFacebook" web application. In this application, any user is capable of registering for an account. Once registered, only users whose accounts weren't disabled are capable of logging in. Once logged in, a user is able to change the password, create a new post, edit one of his/her posts, or add a comment to anyone's post, including his/her own. Some users are not allowed to log in, meaning they cannot use any of the features of the "miniFacebook", because a superuser exists. This superuser is allowed to log into the application without registering (information added directly to the database) and disable/enable a registered user. The last feature of the "miniFacebook" application is that logged-in users can chat with other users online. There are also many security implementations that are required for this application.

In regard to the project accomplishments, all of the design requirements were implemented successfully. Anyone can register an account with the miniFacebook website, and this information is stored in the database, with the password being hashed. Logged-in users can change their password, add/edit/delete a post, add/edit/delete a comment on anyone's post (including their own), and they can chat with other logged-in users. A superuser was also created that has the same abilities as a regular user, but he/she can also enable/disable a user in the database. Disabling a user would prevent that user from being able to log in. On the other hand, enabling a user would allow the user to log in. Lastly, all the security requirements were met. Specific demonstrations of the project design, tests, and security can be seen in the upcoming sections.


# 2. Design

###   Database
The database consists of a "users" table, a "posts" table, and a "comments" table.  

##### Users Table
The "users" table consists of a username, password, and an enabled status. The primary key is the username. This can be seen in the the "users" table below.  

| Field    | Type         | Null | Key | Default | Extra |
|----------|--------------|------|-----|---------|-------|
| username | varchar(50)  | NO   | PRI | NULL    |       |
| password | varchar(100) | NO   |     | NULL    |       |
| enabled  | int(11)      | YES  |     | NULL    |       |

##### Posts Table
The "posts" table consists of the post's identification number, the time when it was posted, the contents of the most, and the person who wrote this post. The primary key is the post's ID number. The owner of the post is a foreign key to the "username" field in the "users" table, so that if the user is removed from the database, then all the posts associated with it are removed as well.

| Field      | Type         | Null | Key | Default           | Extra                       |
|------------|--------------|------|-----|-------------------|-----------------------------|
| posts_id   | int(11)      | NO   | PRI | NULL              | auto_increment              |
| time_stamp | timestamp    | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
| message    | varchar(250) | YES  |     | NULL              |                             |
| owner      | varchar(50)  | YES  | MUL | NULL              |                             |


##### Comments Table
The "comments" table is very similar to the "posts" table with a new fields added. The comment has the ID number of the post that it is attached to, as well as its own ID number. For comments, the "post_id" and "owner" fields are foreign keys to the "post_id" field of the "post" table and "username" field of the "users" table respectively. The primary key of the comments is the "comments_id".

| Field       | Type         | Null | Key | Default           | Extra                       |
|-------------|--------------|------|-----|-------------------|-----------------------------|
| posts_id    | int(11)      | YES  | MUL | NULL              |                             |
| message     | varchar(250) | YES  |     | NULL              |                             |
| time_stamp  | timestamp    | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
| owner       | varchar(50)  | YES  | MUL | NULL              |                             |
| comments_id | int(11)      | NO   | PRI | NULL              | auto_increment              |

###   The user interface, e.g., the Web interface and CSS
Our implementation focused mainly on functionality rather than aesthetics, which is why our mini Facebook page has a very simple interface.

We used very basic CSS styling within the HTML div tags to separate the posts and comments from each other. Our webpage can be seen below.

![](/reportimages/FrontPage.PNG)  
*Figure 1: Front Page of our Mini Facebook*

###   Functionalities of our application

Our application functions as a very basic Facebook type application. Users have to sign up and login before they can interact with others on the website. These users are considered regular users, as they do not have the power to disable other users' accounts and their accounts were created through the registration form. The super user can only be created directly within the website's database, and our database.php file creates this super user upon import. The superuser is the only account that does not have a email associated with it (compared to the regular users who are distingushed with their email domain). Regular accounts do not have access to the button that disables or enables accounts on the webpage. Super users on the other hand will have this button and can disable and enable accounts. 

The newest posts/comments are displayed toward the top of the page, to mimic Facebook which also implements this. Comments and posts can be written directly on the home page (as opposed to having to redirect users to a separate "write post" or "write comment" page). Users can only edit their own posts/comments by clicking on the edit button. A text box will appear for them to edit their post and they will have to hit the confirm button to apply their changes.

# 3. Implementation & security analysis


####   How did you apply the security programming principles in your project?

We applied security programming principles in our project by implementing a lot of concept and mechanisms that were taught to us in class and in the hands-on labs. We made sure to protect both the server side and the client side of this application. We validated input and santized output to protect our application's database, created our application on a HTTPS server to ensure secure data transmission, and protected the user's session to protect their information.

####   Have you used defense in depth and defense in breath principles in your project?

We utilized defense in depth for our application by implementing many different mechanisms of defense. This is especially utilized with input validation. The user input had to be validated through certain checks and then the format of the input had to be validated as well. This was important to prevent any SQL injection attacks or Cross Site Scripting attacks. 

Defense in breath has been utilized in the many technologies of this project. First we protect the data transmission by encrypting it with the HTTPS server, then making sure users are authenticated to their own session in their web browser, and then we had to validate user input to maintain the application's data security, integrity, and safety.

####   What database security principles you have used in your project?

The first step towards database security was access control and database privilage which was to make sure that the application did not use the "root" database account to access the application's database. A compromise to the root user would be dangerous for other database on the system. The next database security measure that had to be accounted for was data access within the database. This had to be done in a secure manner as to not expose information within the database. These SQL injection attacks where prevented using Prepared Statements and input validation. 

####   Is your code robust and defensive? How?

Our code is fairly robust, any form of invalid input will not break our application. Our implementations would throw an alert stating the error and sending the user back to the front page of our website, or have them re-enter some input.

The defensiveness of our application is covered in fields like input validation. All types of checks are made, such as checks if a variable is null or if the data is of a valid format. Our code for the most part is understandable, especially our functions within our database.php which handles the bulk of the application's database queries. Our code performs in a predictable manner, even with unpredictable input.

####   How did you defend your code against known attacks such as including XSS, SQL Injection, CSRF, Session Hijacking

We utilized a lot of the mechanisms and techniques taught to use in class to prevent XSS, SQLi, CSRF, and Session Hijecking.

Cross Site Scripting was prevented with the use of output sanitation. This was done by utilizing the htmlentities() function so that scripts can't be exeucuted.

SQL Injection attacks are combatted with the use of Prepared Statements.

Cross Site Request Forgery is prevented with the use of a secret token that is unique only to the session browser. The attacker cannot access this value or even know this value. 

Session Hijacking is prevented with the use of PHP's session_set_cookies_param() function which will make the session secure. Setting certain parameters (such as $httponly and $secure just to name a few) can help prevent session hijacking. Other measures taken were to encrypt data transmission with the use of HTTPS so that the cookie session ID cannot be stolen.

####   How do you separate the roles of super users and regular users?

Our roles are simply separated by a naming convention. Regular users have their emails associated to their username. This is a very simply implementation, and we would definitely restructure this for professional or commercial use, but for the sake of this project we wanted to keep things simple. We also only allow one single super user, with the username of "nguyenmalek". Our implementation checks if the username is "nguyenmalek" and will grant it the ability to enable/disable users. Since MYSQL database does not allow for duplicate key fields (aka the usernames) this should not be an issue. For future improvements to this system, another field would be added inside the "users" table that will indicate if a user is a "super user" or a "regular user". Again for this project we kept it simple and only allowed for a single super user which is created upon importing the database.sql to the database.


# 4. Demonstration


### User Registration and Login

In the first test case, it will be shown that a user can register a new account with a valid username and password. This information will be saved in the database, with the password being masked. Then, that user can log into the miniFacebook website. This can be seen below in Figure 2-4.


![](/reportimages/Registration.PNG)  
*Figure 2: Registration Form for New User*


![](/reportimages/DbafterUserRegistration.PNG)  
*Figure 3: Database after User Registration*


![](/reportimages/loginafterUserRegistration.PNG)  
*Figure 4: Login after User Registration*


### User Change Password

When logged-in, the user has the ability to write/edit/delete its own post or comment, chat with other logged-in users, or change its password. The first demonstration of these actions is the user's ability to change its own password. When the user wants to change its password, it can click on the "Change Password" link on the homepage. Then, the user is redirected to another page, where the user changes the password. After the change has been submitted, the database updates the password, and the user will have to use the new password at the next login. The process of changing the password and updating the database can be seen below in Figure 5 and Figure 6.


![](/reportimages/passwordChange.PNG)  
*Figure 5: User Changing Password*


![](/reportimages/passwordupdateinDatabase.PNG)  
*Figure 6: Database Update after Password is Changed*


### User Writing a Post
When the user wants to write a post, it can simply write in the text box on the homepage. When the user presses the 'Post' button, the post will remain on the page forever, unless the post gets deleted or the database is imported again. The demonstration of writing a post can be seen below in Figure 7-9.


![](/reportimages/writingaPost.PNG)  
*Figure 7: User Writing a Post*


![](/reportimages/databaseafterPost.PNG)  
*Figure 8: Database Update after Post*


![](/reportimages/viewingPost.PNG)  
*Figure 9: Homepage Update after Post*


### User Editing a Post

If the user changes its mind about what it posted, the user may edit its post. This edit updates the database to the new text, and then the Posts section on the homepage updates to read the edited post. This can be seen below in Figure 10-12.


![](/reportimages/editingPost.PNG)  
*Figure 10: User Editing a Post*


![](/reportimages/databaseafterEdit.PNG)  
*Figure 11: Database Update after Edit*


![](/reportimages/homepageupdateafterEdit.PNG)  
*Figure 12: Homepage Update after Edit*


### User Commenting on a Post

Users are also able to add a comment to anyone's post, including their own. This can be seen below in Figure 13. One can also notice that a user cannot edit or delete a post or comment that came from another user in Figure 13.


![](/reportimages/comments.PNG)  
*Figure 13: User Commenting on its own Post and another User's Post*


### User Deleting a Post
If the user chooses to delete its post on the website, it will disappear from the 'Posts' section of the homepage, as well as the database. Also, all comments written under a deleted post disappear. This can be seen below in Figure 14 and Figure 15. Comments can also be deleted from under a users's post, but it will not affect the post or any other comments in the section. In either case, post and comment(s) are removed from the database.


![](/reportimages/deletePost.PNG)  
*Figure 14: User Deleting Post*


![](/reportimages/deletepostAftermath.PNG)  
*Figure 15: Webpage Update after Deleted Post*


### User Chatting with Other Logged-in Users

If the user chooses talk to other logged-in users, it can click on the link to 'Chat with People!'. When a user clicks on the link, the user is sent to a webpage that operates like a discussion board, where all logged-in users can write chat messages at the same time. An image displaying two users talking to each other in the chat can be seen below in Figure 16 and Figure 17.


![](/reportimages/chatsystem.PNG)  
*Figure 16: Two Users Talking to each Other on Chat System*


![](/reportimages/chatserver.PNG)  
*Figure 17: Server Running the Chat System*


### Superuser

The last implementation of the miniFacebook that is demonstrated is the use of the superuser. The superuser is an account that can log in and use the website, although it can also monitor what other users can use miniFacebook. All users who register an account are allowed to log into miniFacebook and use it, but if the superuser disables the account, the user can no longer log in. The first demonstration will exhibit all registered users being 'enabled', which occurs after registration, allowing them to use miniFacebook. The first image, Figure 18, shows the homepage of the superuser. When comparing this homepage to a regular user's homepage, it can be observed that regular users cannot access the page to enable/disable other users. The other two images, Figure 19 and Figure 20, demonstrate the 'enabled' status of users after registration.


![](/reportimages/superuserHomepage.PNG)  
*Figure 18: Homepage of Superuser Account*


![](/reportimages/databaseusersEnabled.PNG)  
*Figure 19: All Users are Enabled after They Register*


![](/reportimages/enablePage.PNG)  
*Figure 20: Enable/Disable Webpage*


#### Disabling User

However, from the enabled/disabled webpage, the superuser can disable a user, if he/she wants to. When the superuser disables a user, that user cannot log in anymore. This demonstration can be seen below in Figure 21-23.


![](/reportimages/disablingUsers.PNG)  
*Figure 21: Superuser Disabling Two Users*


![](/reportimages/disablingusersDatabase.PNG)  
*Figure 22: Database Update after User Disablement*


![](/reportimages/disabledUser.PNG)  
*Figure 23: maleks2@aol.com Disabled from miniFacebook*


#### Enabling User

When the superuser decides to enable the user accounts again, they will be able to log into the miniFacebook again. This can be seen below in Figure 24-26.


![](/reportimages/enablingUsers.PNG)  
*Figure 24: Superuser Enabling Two Users*


![](/reportimages/enablingusersDatabase.PNG)  
*Figure 25: Database Update after User Enablement*


![](/reportimages/enabledUser.PNG)  
*Figure 26: maleks2@aol.com can Access miniFacebook*


### CSRF Attack

A CSRF Attack is simulated using a "fake" malicious link that we created that attempts to change a logged in user's password through the URL.
A victim may be tricked into clicking this link, and if the website does not protect against this attack, it can mean that the victim can no longer log into their account anymore. An example of when a CSRF attack is attempted and stopped can be seen below in Figure 27.

![](/reportimages/CSRF2.png)  
*Figure 27: CSRF Attack Attempt that's Prevented*

# Appendix


#### addnewuser.php
```php
<?php
	require "database.php";
	$username = sanitize_input($_POST["username"]);
	$password = sanitize_input($_POST["password"]);
    //echo "username: $username";
    //echo "password: $password";
	if (!validateUsername($username) or !validatePassword($password)) {
		echo "<script>alert(Please enter valid username/password');</script>";
		header("Refresh:0; url=registrationform.php");
	}

	//echo "DEBUG:addnewuser.php>username=$username;password=$password";
	if(addnewuser($username,$password)) {
		//echo "DEBUG:addnewuser.php> $username is added";
		echo "<script>alert('Your new account is successfully registered. Please login!');</script>";
		header("Refresh:0; url=form.php");
	} else {
		//echo "DEBUG:addnewuser.php> $username was not added";
		echo "<script>alert('Something was wrong with your registration. Please try again.');</script>";
		header("Refresh:0 url=registrationform.php");
	}
?>

```
#### changepasswordform.php
```php
<?php
	require "session_auth.php";
  $rand=bin2hex(openssl_random_pseudo_bytes(16));
  $_SESSION["nocsrftoken"] = $rand;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Change Password page - SecAD</title>
</head>
<body>
      	<h1>A Change Password form, SecAD</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="changepassword.php" method="POST" class="form login">
                Username:<!--<input type="text" class="text_field" name="username">-->
                <?php echo htmlentities($_SESSION["username"]); ?> 
                <br>
                <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                New Password: <input type="password" class="text_field" name="newpassword" /> <br>
                <button class="button" type="submit">
                  Change Password
                </button>
          </form>

</body>
</html>

<a href="index.php">Home</a> | <a href="logout.php">Logout</a>
```

#### changepassword.php
```php
<?php
	require "session_auth.php";
	require "database.php";
	$username=$_SESSION["username"];
	$newpassword=$_REQUEST["newpassword"];
	$nocsrftoken = $_POST["nocsrftoken"];
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION["nocsrftoken"])) {
		echo "<script>alert('Cross-site request forgery is detected!');</script>";
		header("Refresh:0; url=logout.php");
		die();
	}
	if (isset($username) AND isset($newpassword)) {
		//echo "DEBUG:changepassword.php->Got: username=$username;newpassword=$newpassword\n<br>";
		if (changepassword($username, $newpassword)) {
			echo "<h4>The new password has been set.</h4>";
		} else {
			echo "<h4>Error: Cannot change the password.</h4>";
		}
	} else {
		echo "No provided username/password to change.";
		exit();
	}
?>
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>
```

#### chat.php
```php
<?php
    require("session_auth.php");
    $username = $_SESSION["username"];
?>

<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<script src="https://www.nguyen-malek.miniFacebook.com:4430/socket.io/socket.io.js"></script>
<script>

var username = <?php echo json_encode($username); ?>;

function startTime() {
    document.getElementById('clock').innerHTML = new Date();
    setTimeout(startTime, 500);
}
if (window.WebSocket) {
 console.log("HTML5 WebSocket is supported");
} else {
  alert("HTML5 WebSocket is not supported");
}
//var myWebSocket = new WebSocket('ws://echo.websocket.org');
//var myWebSocket = io.connect('192.168.56.103:4430');
var myWebSocket = io.connect('www.nguyen-malek.miniFacebook.com:4430');
myWebSocket.onopen = function() { 
	console.log('WebSocket opened'); 
}

var sanitizeHTML = function(str){
    var temp = document.createElement('div');
    temp.textContent = str;
    return temp.innerHTML;
}

myWebSocket.on("message", function(msg) {
    console.log('Received from server: '+ msg);
    document.getElementById("receivedmessage").innerHTML += sanitizeHTML(msg) + "<br>";
});

myWebSocket.on("typing", function(name) {
    console.log(name);
    document.getElementById("typing").innerHTML = sanitizeHTML(name) + " is typing... <br>";
    setTimeout(function(){document.getElementById("typing").innerHTML = "<br>";},2000);
});

myWebSocket.onclose = function() { 
	console.log('WebSocket closed');
}

function doSend(msg){
    if (myWebSocket) {
        //myWebSocket.send(msg);
        msg = username + ": " + msg;
        myWebSocket.emit("message", msg);
        console.log('Sent to server: ',  msg);
    }
}
function entertoSend(e){
  //alert("keycode =" + e.keyCode); 
  if(e.keyCode==13){//enter key
    doSend(document.getElementById("message").value);
    document.getElementById("message").value = "";
  }
}
</script>

<a href="index.php">Home</a> |
<a href="logout.php">Logout</a>  
<br>

<body onload="startTime()">
Current time: <div id="clock"></div>

Type message and enter to send: <input type = "text" id="message" size = "30" onkeypress="entertoSend(event)" onkeyup="myWebSocket.emit('typing', username)"/>
<br>
<div id="typing"></div>
Message from server:
<hr>
<div id = "receivedmessage"></div>


</body>
</html>
```

#### database.php
```php
<?php
	$mysqli = new mysqli('localhost', 'nguyenmalek', 'mysecretP4$$', 'secadproject');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_error);
		exit();
	}
	function changepassword($username, $newpassword) {
		if (validatePassword($newpassword) == FALSE) {
			return FALSE;
		}
		global $mysqli;
		$prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
		//echo "DEBUG>prepared_sql= $prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ss", $newpassword, $username);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function addnewuser($username, $password) {
		if (validateUsername($username) == FALSE or validatePassword($password) == FALSE) {
			return FALSE;
		}
		global $mysqli;
		$prepared_sql = "INSERT INTO users VALUES (?, password(?),1);";
		//echo "DEBUG>prepared_sql= $prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ss", $username, $password);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function edit($post_id, $message, $type) {
		if(empty($message) or strlen($message) < 1 or strlen($message) > 3000) {
			return FALSE;
		}
		global $mysqli;
        $prepared_sql = "UPDATE $type SET message=? WHERE " . $type . "_id= ?;";
		//echo "DEBUG>prepared_sql= $prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("si", $message, $post_id);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function deleteitem($post_id, $type) {
		global $mysqli;
        //$prepared_sql = "SET FOREIGN_KEY_CHECKS = 0; DELETE FROM $type WHERE " . $type . "_id= ?; SET FOREIGN_KEY_CHECKS = 1;";
        $prepared_sql = "DELETE FROM $type WHERE " . $type . "_id= ?;";
		//echo "DEBUG>prepared_sql= $prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("i", $post_id);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function changeusersettings($checklist){
		global $mysqli;
		$prepared_sql = "UPDATE users SET enabled=0 WHERE username != 'nguyenmalek';";
    	if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    	if(!$stmt->execute()) return FALSE;

    	foreach ($checklist as $check => $user) {
    		$prepared_sql = "UPDATE users SET enabled=1 WHERE username=?;";
    		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    		$stmt->bind_param('s', $user);
    		if(!$stmt->execute()) return FALSE;
    	}
    	return TRUE;
	}

	function writepost($owner, $message) {
		if(empty($message) or strlen($message) < 1 or strlen($message) > 3000) {
			return FALSE;
		}
		global $mysqli;
        $time_stamp = date('Y-m-d H:i:s');
		$prepared_sql = "INSERT INTO posts(time_stamp, owner, message) VALUES (?, ?, ?);";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("sss", $time_stamp, $owner, $message);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function writecomment($owner, $message, $post_id) {
		if(empty($message) or strlen($message) < 1 or strlen($message) > 3000) {
			return FALSE;
		}
		global $mysqli;
        $time_stamp = date('Y-m-d H:i:s');
		//echo "DEBUG>time_stamp= $time_stamp\n";
		$prepared_sql = "INSERT INTO comments(posts_id, message, time_stamp, owner) VALUES (?, ?, ?, ?);";
        //echo "DEBUG > prepared_sql= $prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("isss", $post_id, $message, $time_stamp, $owner);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
	}

	function validateUsername($username) {
		$username = sanitize_input($username);
		if(empty($username) or strlen($username) < 6 or strlen($username) > 40) {
			return FALSE;
		}
		return TRUE;
	}

	function validatePassword($password) {
		$password = sanitize_input($password);
		if(empty($password) or strlen($password) < 6 or strlen($password) > 40) {
			return FALSE;
		}
		return TRUE;
	}

	function sanitize_input($input) {
		$input = trim($input);
		$input = stripslashes($input);
		$input = htmlspecialchars($input);
        //echo "sanitize_input: $input";
		return $input;
	}
?>
```

#### database.sql
```sql
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS posts;
CREATE TABLE posts(
    posts_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    time_stamp TIMESTAMP,
    message varchar(250),
    owner varchar(50),
    FOREIGN KEY (owner) REFERENCES users(username) ON DELETE CASCADE
);

DROP TABLE IF EXISTS comments;
CREATE TABLE comments(
    posts_id int,
    message varchar(250),
    time_stamp TIMESTAMP,
    owner varchar(50),
    comments_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    FOREIGN KEY (owner) REFERENCES users(username) ON DELETE CASCADE,
    FOREIGN KEY (posts_id) REFERENCES posts(posts_id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS users;
CREATE TABLE users(
    username varchar(50) PRIMARY KEY,
    password varchar(100) NOT NULL,
    enabled int
);

INSERT INTO users (username, password, enabled) VALUES ('nguyenmalek', 'mysecretP4$$', 1);
UPDATE users SET password=password('mysecretP4$$') WHERE username='nguyenmalek';

SET FOREIGN_KEY_CHECKS=1;
```

#### delete.php
```php
<?php
	require "session_auth.php";
	require "database.php";
	$username=$_SESSION["username"];
	$post_id=$_GET["post_id"];
	$type = $_GET["type"];
	
	if (isset($username) AND isset($post_id) AND isset($type)) {
		if (deleteitem($post_id, $type)) {
			echo "<script>alert('The post/comment has been deleted.');</script>";
		} else {
			echo "<script>alert('Error: Cannot delete the post/comment.');</script>";
		}
	} else {
		echo "<script>alert('No provided post/comment to delete.');</script>";
		exit();
	}
	header("Refresh:0; url=index.php");
?>
```

#### editform.php
```php
<?php
	require "session_auth.php";
	//if isset($_GET["message"]) {
		$message = $_GET["message"];
	//}
	//if isset($_GET["post_id"]) {
        $post_id =  $_GET["post_id"];
    //}
        $type = $_GET["type"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Edit a Post/Comment - SecAD</title>
</head>
<body>
      	<h1>Edit a Post/Comment:</h1>

        <form action="edit.php?post_id=<?php echo htmlentities($post_id); ?>&type=<?php echo htmlentities($type); ?>" method="POST" >
	   			What's on your mind? </br>
	   			<textarea rows="5" cols="80" name="message"><?php echo htmlentities($message) ?></textarea>
				<button class="button" type="submit">
                  Update
                </button>
			</form>

</body>
</html>
</br>
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>
```

#### edit.php
```php
<?php
	require "session_auth.php";
	require "database.php";
	$username=$_SESSION["username"];
	$message=$_REQUEST["message"];
	$post_id=$_POST["post_id"];
	$type=$_POST["type"];

    echo "$username, $message, $post_id, $type";

	if (isset($username) AND isset($message) AND isset($post_id) AND isset($type)) {
		if (!edit($post_id, $message, $type)) {
			echo "<script>alert('Error: Post could not be updated.');</script>";
		}
	} else {
        echo "<script>alert('Error: provided no post.');</script>";
		exit();
	}
    header("Refresh:0; url=index.php");
?>
```

#### enabledisableform.php
```php
<?php
	require "session_auth.php";

    $mysqli = connect2database();
    $prepared_sql = "SELECT username, enabled FROM users WHERE username != 'nguyenmalek'";
    if(!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement Error";
    if(!$stmt->execute()) echo "Execute Error";
    $username = NULL; $enabled = NULL;
    if(!$stmt->bind_result($username, $enabled)) echo "Binding failed";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Enable/Disable User page - SecAD</title>
</head>
<body>
      	<h1>Enable/Disable User form, SecAD</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa");
?>

			<form action="enabledisable.php" method="POST" >
				<br>
				Mark for enable and unmark for disable (The beginning marks designate the current status of the account):
				<br>
                <?php
                	$i = 0;
                	while($stmt->fetch()){
                		if ($enabled == 1) {
        		?>		
        					<input type="checkbox" id="<?php echo htmlentities($i)?>" name="checkedlist[]" value="<?php echo htmlentities($username)?>" checked >
  							<label for="<?php echo htmlentities($i)?>" ><?php echo htmlentities($username); ?></label><br>
        		<?php
        				} else {
        		?>
        					<input type="checkbox" id="<?php echo htmlentities($i)?>" name="checkedlist[]" value="<?php echo htmlentities($username)?>" >
  							<label for="<?php echo htmlentities($i)?>" ><?php echo htmlentities($username); ?></label><br>
        		<?php
        				}
        				$i = $i + 1;
    				}
                ?>
                <button class="button" type="submit">
                  Submit
                </button>
         	</form>

</body>
</html>
<br>

<a href="index.php">Home</a> | <a href="logout.php">Logout</a>

<?php
function connect2database(){
    $mysqli = new mysqli('localhost',
              'nguyenmalek' /*database username */,
              'mysecretP4$$' /* database password */,
              'secadproject' /* database name*/);
    if($mysqli->connect_errno){
        printf("Database connection failed: %s\n", $mysqli->connect_error);
        exit();
    }
    return $mysqli;
}
?>
```

#### enabledisable.php
```php
<?php
	require "session_auth.php";
	require "database.php";
	$username=$_SESSION["username"];
	$checkedlist = $_POST["checkedlist"];

	if (isset($username) AND isset($checkedlist)) {
		//echo "<br>DEBUG:enabledisable.php->Got: username=$username;checkedlist=$checkedlist\n<br>";
		if (changeusersettings($checkedlist)) {
			echo "<script>alert('The new enable/disable setting has been set.');</script>";
		} else {
			echo "<script>alert('Error: Cannot enable/disable the user');</script>";
		}
	} else {
		echo "<script>alert('You cannot disable every user from the website! Try again');</script>";
	}
	header("Refresh:0; url=index.php");
?>
```

#### form.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
      	<h1>A Simple login form, SecAD</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>

        <h2> New User?, Create an account! </h2>
        <a href="registrationform.php">Create Account!</a>

</body>
</html>
```

#### index.php
```php
<?php
	$lifetime = 15 * 60; // 15 minutes
	$path = "/miniFB";
	$domain = "www.nguyen-malek.miniFacebook.com"; // note your IP or hostname
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();
	if (isset($_POST["username"]) and isset($_POST["password"]))  {
		if (securechecklogin($_POST["username"],$_POST["password"])) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
		}else{
			echo "<script>alert('Invalid username/password, or your account has been disabled');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}
	if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not logged in. Please log in first'); </script>";
		header("Refresh:0; url=form.php");
		die();
	}
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
		echo "<script>alert('Session hijacking is detected!');</script>";
		header("Refresh:0; url=form.php");
		die();
	}

?>

<h1> Welcome <?php echo htmlentities($_SESSION["username"]); ?> !</h1>
<a href="changepasswordform.php">Change Password</a> | 
<a href="logout.php">Logout</a> | 
<a href="chat.php">Chat with People!</a>
<?php
    if (strcmp($_SESSION["username"], "nguyenmalek") == 0) {
?>
 | <a href="enabledisableform.php">Enable/Disable User(s)</a>
<?php
    }
?>
<br>
<br>

<body>
<?php

    require("writepostform.php");
    echo "<h2> Posts: </h2>";

    // Display all the contents of the "posts" table
    $mysqli = connect2database();
    $prepared_sql = "SELECT posts_id, time_stamp, message, owner FROM posts ORDER BY posts_id DESC;";
    if(!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement Error";
    if(!$stmt->execute()) echo "Execute Error";
    $post_id = NULL; $time_stamp = NULL; $message = NULL; $owner = NULL;
    if(!$stmt->bind_result($post_id, $time_stamp, $message, $owner)) echo "Binding failed";
?>

<?php
    //Getting all the posts in the database to display
    $btn_id = 0;
    while($stmt->fetch()){
?>
        
        <div style="background-color:#A9D0F5">
            <h4><b><?php echo htmlentities($owner)?></b> posted @ <?php echo htmlentities($time_stamp)?>:</h4>
            <?php displayedits($btn_id, $message, $post_id, 'posts'); ?>
            <div id="<?php echo "post_". htmlentities($btn_id) ?>">
                <?php echo htmlentities($message)?>
            </div>
            <br>
            <br>
            <?php showEditDeletebuttons($owner, $message, $post_id, 'posts', $btn_id);?>
        </div>

        <div style="background-color:lightblue">
    <?php
        $btn_id++;
        echo "<b>Comments:</b><br>";
        printcomments($post_id);
        echo "<br>";
        require("writecommentform.php");
    }
    ?>
        </div>

<?php
    function printcomments($post_id){
        global $btn_id;

        $mysqli = connect2database();
        $prepared_sql = "SELECT message, time_stamp, owner, comments_id FROM comments WHERE posts_id=? ORDER BY comments_id DESC;";
        if(!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement Error";
        $stmt->bind_param('i', $post_id);
        if(!$stmt->execute()) echo "Execute Error";
        $time_stamp = NULL; $message = NULL; $owner = NULL; $comments_id = NULL;
        if(!$stmt->bind_result($message, $time_stamp, $owner, $comments_id)) echo "Binding failed";
        //show buttons for comments
        while($stmt->fetch()){
            displayedits($btn_id, $message, $comments_id, 'comments');
?>

            <div id="<?php echo "post_". htmlentities($btn_id) ?>">
                <?php echo htmlentities($time_stamp) . " - <b>" . htmlentities($owner) . "</b>" . ": " . htmlentities($message) . "<br>";
                ?>
            </div>
<?php
            showEditDeletebuttons($owner, $message, $comments_id, 'comments', $btn_id++);
        }
    }
?>
</body>

<?php
    function showEditDeletebuttons($owner, $message, $post_id, $type, $btn_id){
        if(strcmp($owner, $_SESSION["username"]) == 0){
?>
    <button onclick = "ShowForm(this)" id = "<?php echo htmlentities($btn_id);?>">Edit</button> 

            <a href = "delete.php?post_id=<?php echo htmlentities($post_id); ?>&type=<?php echo htmlentities($type); ?>" onclick="return confirm('Are you sure you want to delete?')" class="del_btn">Delete</a> |
<!--
            <a href = "editform.php?message=<?php echo htmlentities($message); ?>&post_id=<?php echo htmlentities($post_id);?>&type=<?php echo htmlentities($type); ?>" class="edit_btn">Edit</a> | 
            <a href = "delete.php?post_id=<?php echo htmlentities($post_id); ?>&type=<?php echo htmlentities($type); ?>" onclick="return confirm('Are you sure you want to delete?')" class="del_btn">Delete</a> |
-->

        <script>
            function ShowForm(currBtnObj){
                var btnID = currBtnObj.id;
                var editEle = document.getElementById("edit_" + btnID);
                var postEle = document.getElementById("post_" + btnID);

                if(editEle.style.display === "none"){
                    editEle.style.display = "block";
                    postEle.style.display = "none";
                 
                }else{
                    editEle.style.display = "none";
                    postEle.style.display = "block";
                 
                }
            }
        </script>
<?php
        }
    }
?>

<?php
  function securechecklogin($username, $password){
    $mysqli = connect2database();
    $prepared_sql = "SELECT * FROM users WHERE enabled=1 AND username= ? " .
                    " AND password=password(?);";
    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";
    $stmt->bind_param("ss", $username,$password);
    if(!$stmt->execute()) echo "Execute Error";
    if(!$stmt->store_result()) echo "Store_result Error";
    $result = $stmt;

    if ($result->num_rows == 1)
        return TRUE;
    
    return FALSE;
  }
?>

<?php
    function connect2database(){
        $mysqli = new mysqli('localhost',
                  'nguyenmalek' /*database username */,
                  'mysecretP4$$' /* database password */,
                  'secadproject' /* database name*/);
        if($mysqli->connect_errno){
            printf("Database connection failed: %s\n", $mysqli->connect_error);
            exit();
        }
        return $mysqli;
    }

?>

<?php
    function displayedits($btn_id, $message, $post_id, $type){
?>
        <div id = "<?php echo "edit_". htmlentities($btn_id);?>" style="display: none">
            <!--<form action = "edit.php?message=<?php echo htmlentities($message); ?>&post_id=<?php echo htmlentities($post_id);?>&type=<?php echo htmlentities($type); ?>" class="edit_btn" method="POST"> -->
            <form action = "edit.php" class="edit_btn" method="POST">
                <textarea rows="1" cols="30" name="message"><?php echo htmlentities($message) ?></textarea>
                <input type="hidden" name="post_id" value="<?php echo htmlentities($post_id) ?>"/>
                <input type="hidden" name="type" value="<?php echo htmlentities($type) ?>"/>
                <button class="button" type="submit">
                Edit 
                </button>
            </form>
        </div>
<?php
    }
?>
```

#### logout.php
```php
<?php
	session_start();
	session_destroy();
?>
<p> You are logged out! </p>

<a href="form.php">Log in again</a>
```

#### registrationform.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Registration page - SecAD</title>
</head>
<body>
      	<h1>A Simple Registration form, SecAD</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="addnewuser.php" method="POST" class="form login"> 
                Username:
                <input type="text" class="text_field" name="username" 
                  pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$"
                  title="Please enter a valid email as username"
                  placeholder="Your email address"
                  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title:'');"/> <br>

                Password: 
                <input type="password" class="text_field" name="password" required
                  placeholder="Enter password"
                  pattern="^(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&])(\w!@#$%^&]{8,}$"
                  title="Password must have at least 8 characters with one uppercase letter, one lowercase letter, one number, and one special character (!@#$%^&)"
                  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"
                  onkeyup="document.forms[0].confirmpassword.pattern = RegExp(this.value);" /> <br>
                <!--Confirm Password: <input type="password" class="text_field" name="confirmpassword"
                  placeholder="Confirm your password" required
                  title="Password does not match"
                  onkeyup="this.setCustomValidity(this.validity.patternMismatch?this.title: '');" /> <br>
-->
                <button class="button" type="submit">
                  Sign Up
                </button>
          </form>

</body>
</html>
```

#### session_auth.php
```php
<?php
	$lifetime = 15 * 60; // 15 minutes
	$path = "/miniFB";
	$domain = "www.nguyen-malek.miniFacebook.com"; // note your IP or hostname
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();

	if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not logged in. Please log in first'); </script>";
		header("Refresh:0; url=form.php");
		die();
	}
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
		echo "<script>alert('Session hijacking is detected!');</script>";
		header("Refresh:0; url=form.php");
		die();
	}
?>
```

#### socketio-chatserver.js
```js
/* A Simple HTTPS server with socket.IO in Node.js
   by Phu Phung for SecAD
   */
/* ensure that you have the key and certificate files
   copied to the /etc/ssl folder as in Lab 6.
   change the file name accordingly.*/
var https = require('https'), fs = require('fs'), xssfilter = require("xss");
var sslcertificate  = {
  key: fs.readFileSync('/etc/ssl/secad.key'),
  cert: fs.readFileSync('/etc/ssl/secad.crt') //ensure you have these two files
};
var httpsServer = https.createServer(sslcertificate,httphandler);
var socketio = require('socket.io')(httpsServer);


httpsServer.listen(4430); //cannot use 443 as since it reserved for Apache HTTPS
console.log("HTTPS server is listening on port 4430");

function httphandler (request, response) {
  //console.log("URL requested = " + request.url);
  //read the websocketchatclient.html file and
  //to create a HTTP Reponse regardless of the requests
  response.writeHead(200); // 200 OK
  var clientUI_stream = fs.createReadStream('./chatclient.html');
  clientUI_stream.pipe(response);
}

socketio.on('connection', function (socketclient) {
  console.log("A new socket.IO client is connected: "+ socketclient.client.conn.remoteAddress+
               ":"+socketclient.id);
    socketclient.on("message", (data) => {
        console.log("received data: " + data);
        var data = xssfilter(data);
        socketio.emit("message", data);
    });

    socketclient.on("typing", (user) => {
        //console.log("Someone is typing..." + user);
        socketclient.broadcast.emit("typing", user);
    });

});
```

#### writecommentform.php
```php
<!DOCTYPE html>
<?php
    require "session_auth.php";
    if(isset($_GET["post_id"])){
        $post_id =  $_GET["post_id"];
    }
?>
<html lang="en">
<head>
  <meta charset="utf-8">
</head>
<body>

    
    <!--<button onclick="showForm()"> Write a comment. </button>
    <div id="commentform" style="display: none;">-->
    <div id="commentform" style="display:inline;">
        <form action="writecomment.php?post_id=<?php echo htmlentities($post_id); ?>" method="POST" style="display: inline;">
            <textarea rows="1" cols="30" name="usercomment"></textarea>
            <button class="button" type="submit">
             Comment 
            </button>
        </form>
    </div>

    <script>
        function showForm(){
            var ele = document.getElementById("commentform");
            if(ele.style.display === "none"){
                ele.style.display = "block";
            }else{
                ele.style.display = "none";
            }
        }
    </script>

</body>
</html>
</br>
<!--
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>
-->
```

#### writecomment.php
```php
<?php
	require "database.php";
	require "session_auth.php";
	$username=$_SESSION["username"];
	$usercomment=$_REQUEST["usercomment"];
    $post_id=$_GET['post_id'];

	if (isset($username) AND isset($usercomment) AND isset($post_id)) {
		if (!writecomment($username, $usercomment, $post_id)) {
			echo "<script>alert('The post was not stored in the database');</script>";
		} 
	} else {
        echo "<script>alert('Error gathering the username or post.');</script>";
		exit();
	}
    header("Refresh:0; url=index.php");
?>
```

#### writepostform.php
```php
<!DOCTYPE html>
<?php
    require "session_auth.php";
?>
<html lang="en">
<head>
  <meta charset="utf-8">
</head>
<body>
    <form action="writepost.php" method="POST" style="display:inline" >
        <b>What's on your mind?</b> </br>
        <textarea rows="5" cols="50" name="userpost"></textarea>
        <button class="button" type="submit">
          Post
        </button>
    </form>
</body>
</html>
</br>
```

#### writepost.php
```php
<?php
	require "database.php";
	require "session_auth.php";
	$username=$_SESSION["username"];
	$userpost=$_REQUEST["userpost"];

	if (isset($username) AND isset($userpost)) {
		if (!writepost($username, $userpost)) {
			echo "<script>alert('Error: The post was not stored in the database.');</script>";
		}
	} else {
        echo "<script>alert('Error gathering the username or post.');</script>";
		exit();
	}
    header("Refresh:0; url=index.php");
?>
```
