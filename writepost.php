<?php
	require "database.php";
	require "session_auth.php";
	$username=$_SESSION["username"];
	$userpost=$_REQUEST["userpost"];

	if (isset($username) AND isset($userpost)) {
		if (!writepost($username, $userpost)) {
			echo "<script>alert('Error: The post was not stored in the database.');</script>";
		}
	} else {
        echo "<script>alert('Error gathering the username or post.');</script>";
		exit();
	}
    header("Refresh:0; url=index.php");
?>
