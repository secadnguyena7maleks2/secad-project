<?php
	require "session_auth.php";
	$message = $_GET["message"];
    $post_id =  $_GET["post_id"];
    $type = $_GET["type"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Edit a Post/Comment - SecAD</title>
</head>
<body>
      	<h1>Edit a Post/Comment:</h1>

        <form action="edit.php?post_id=<?php echo htmlentities($post_id); ?>&type=<?php echo htmlentities($type); ?>" method="POST" >
	   			What's on your mind? </br>
	   			<textarea rows="5" cols="80" name="message"><?php echo htmlentities($message) ?></textarea>
				<button class="button" type="submit">
                  Update
                </button>
			</form>

</body>
</html>
</br>
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>
