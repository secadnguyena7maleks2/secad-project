<?php
	require "session_auth.php";

    $mysqli = connect2database();
    $prepared_sql = "SELECT username, enabled FROM users WHERE username != 'nguyenmalek'";
    if(!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement Error";
    if(!$stmt->execute()) echo "Execute Error";
    $username = NULL; $enabled = NULL;
    if(!$stmt->bind_result($username, $enabled)) echo "Binding failed";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Enable/Disable User page - SecAD</title>
</head>
<body>
      	<h1>Enable/Disable User form, SecAD</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa");
?>

			<form action="enabledisable.php" method="POST" >
				<br>
				Mark for enable and unmark for disable (The beginning marks designate the current status of the account):
				<br>
                <?php
                	$i = 0;
                	while($stmt->fetch()){
                		if ($enabled == 1) {
        		?>		
        					<input type="checkbox" id="<?php echo htmlentities($i)?>" name="checkedlist[]" value="<?php echo htmlentities($username)?>" checked >
  							<label for="<?php echo htmlentities($i)?>" ><?php echo htmlentities($username); ?></label><br>
        		<?php
        				} else {
        		?>
        					<input type="checkbox" id="<?php echo htmlentities($i)?>" name="checkedlist[]" value="<?php echo htmlentities($username)?>" >
  							<label for="<?php echo htmlentities($i)?>" ><?php echo htmlentities($username); ?></label><br>
        		<?php
        				}
        				$i = $i + 1;
    				}
                ?>
                <button class="button" type="submit">
                  Submit
                </button>
         	</form>

</body>
</html>
<br>

<a href="index.php">Home</a> | <a href="logout.php">Logout</a>

<?php
function connect2database(){
    $mysqli = new mysqli('localhost',
              'nguyenmalek' /*database username */,
              'mysecretP4$$' /* database password */,
              'secadproject' /* database name*/);
    if($mysqli->connect_errno){
        printf("Database connection failed: %s\n", $mysqli->connect_error);
        exit();
    }
    return $mysqli;
}
?>