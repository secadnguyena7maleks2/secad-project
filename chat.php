<?php
    require("session_auth.php");
    $username = $_SESSION["username"];
?>

<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<script src="https://www.nguyen-malek.miniFacebook.com:4430/socket.io/socket.io.js"></script>
<script>

var username = <?php echo json_encode($username); ?>;

function startTime() {
    document.getElementById('clock').innerHTML = new Date();
    setTimeout(startTime, 500);
}
if (window.WebSocket) {
 console.log("HTML5 WebSocket is supported");
} else {
  alert("HTML5 WebSocket is not supported");
}
var myWebSocket = io.connect('www.nguyen-malek.miniFacebook.com:4430');
myWebSocket.onopen = function() { 
	console.log('WebSocket opened'); 
}

var sanitizeHTML = function(str){
    var temp = document.createElement('div');
    temp.textContent = str;
    return temp.innerHTML;
}

myWebSocket.on("message", function(msg) {
    console.log('Received from server: '+ msg);
    document.getElementById("receivedmessage").innerHTML += sanitizeHTML(msg) + "<br>";
});

myWebSocket.on("typing", function(name) {
    console.log(name);
    document.getElementById("typing").innerHTML = sanitizeHTML(name) + " is typing... <br>";
    setTimeout(function(){document.getElementById("typing").innerHTML = "<br>";},2000);
});

myWebSocket.onclose = function() { 
	console.log('WebSocket closed');
}

function doSend(msg){
    if (myWebSocket) {
        msg = username + ": " + msg;
        myWebSocket.emit("message", msg);
        console.log('Sent to server: ',  msg);
    }
}
function entertoSend(e){
  if(e.keyCode==13){//enter key
    doSend(document.getElementById("message").value);
    document.getElementById("message").value = "";
  }
}
</script>

<a href="index.php">Home</a> |
<a href="logout.php">Logout</a>  
<br>

<body onload="startTime()">
Current time: <div id="clock"></div>

Type message and enter to send: <input type = "text" id="message" size = "30" onkeypress="entertoSend(event)" onkeyup="myWebSocket.emit('typing', username)"/>
<br>
<div id="typing"></div>
Message from server:
<hr>
<div id = "receivedmessage"></div>


</body>
</html>
