<?php
	$lifetime = 15 * 60; // 15 minutes
	$path = "/miniFB";
	$domain = "www.nguyen-malek.miniFacebook.com"; // note your IP or hostname
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();
	if (isset($_POST["username"]) and isset($_POST["password"]))  {
		if (securechecklogin($_POST["username"],$_POST["password"])) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
		}else{
			echo "<script>alert('Invalid username/password, or your account has been disabled');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}
	if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not logged in. Please log in first'); </script>";
		header("Refresh:0; url=form.php");
		die();
	}
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
		echo "<script>alert('Session hijacking is detected!');</script>";
		header("Refresh:0; url=form.php");
		die();
	}

?>

<h1> Welcome <?php echo htmlentities($_SESSION["username"]); ?> !</h1>
<a href="changepasswordform.php">Change Password</a> | 
<a href="logout.php">Logout</a> | 
<a href="chat.php">Chat with People!</a>
<?php
    if (strcmp($_SESSION["username"], "nguyenmalek") == 0) {
?>
 | <a href="enabledisableform.php">Enable/Disable User(s)</a>
<?php
    }
?>
<br>
<br>

<body>
<?php

    require("writepostform.php");
    echo "<h2> Posts: </h2>";

    // Display all the contents of the "posts" table
    $mysqli = connect2database();
    $prepared_sql = "SELECT posts_id, time_stamp, message, owner FROM posts ORDER BY posts_id DESC;";
    if(!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement Error";
    if(!$stmt->execute()) echo "Execute Error";
    $post_id = NULL; $time_stamp = NULL; $message = NULL; $owner = NULL;
    if(!$stmt->bind_result($post_id, $time_stamp, $message, $owner)) echo "Binding failed";
?>

<?php
    //Getting all the posts in the database to display
    $btn_id = 0;
    while($stmt->fetch()){
?>
        
        <div style="background-color:#A9D0F5">
            <h4><b><?php echo htmlentities($owner)?></b> posted @ <?php echo htmlentities($time_stamp)?>:</h4>
            <?php displayedits($btn_id, $message, $post_id, 'posts'); ?>
            <div id="<?php echo "post_". htmlentities($btn_id) ?>">
                <?php echo htmlentities($message)?>
            </div>
            <br>
            <?php showEditDeletebuttons($owner, $message, $post_id, 'posts', $btn_id);?>
        </div>

        <div style="background-color:white">
    <?php
        $btn_id++;
        echo "<b>Comments:</b><br>";
        printcomments($post_id);
        echo "<br>";
        require("writecommentform.php");
    }
    ?>
        </div>

<?php
    function printcomments($post_id){
        global $btn_id;

        $mysqli = connect2database();
        $prepared_sql = "SELECT message, time_stamp, owner, comments_id FROM comments WHERE posts_id=? ORDER BY comments_id DESC;";
        if(!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement Error";
        $stmt->bind_param('i', $post_id);
        if(!$stmt->execute()) echo "Execute Error";
        $time_stamp = NULL; $message = NULL; $owner = NULL; $comments_id = NULL;
        if(!$stmt->bind_result($message, $time_stamp, $owner, $comments_id)) echo "Binding failed";
        //show buttons for comments
        while($stmt->fetch()){
?>
            <div>
<?php
                showEditDeletebuttons($owner, $message, $comments_id, 'comments', $btn_id);
                displayedits($btn_id, $message, $comments_id, 'comments');
?>

                <span id="<?php echo "post_". htmlentities($btn_id) ?>">
                    <?php echo htmlentities($time_stamp) . " - <b>" . htmlentities($owner) . "</b>" . ": " . htmlentities($message) . "<br>";
                    ?>
                </span>
            </div>
<?php
            $btn_id++;
        } }
?>
</body>

<?php
    function showEditDeletebuttons($owner, $message, $post_id, $type, $btn_id){
        if(strcmp($owner, $_SESSION["username"]) == 0){
?>
    <button onclick = "ShowForm(this)" id = "<?php echo htmlentities($btn_id);?>">Edit</button> 

            <a href = "delete.php?post_id=<?php echo htmlentities($post_id); ?>&type=<?php echo htmlentities($type); ?>" onclick="return confirm('Are you sure you want to delete?')" class="del_btn">Delete</a> |

        <script>
            function ShowForm(currBtnObj){
                var btnID = currBtnObj.id;
                var editEle = document.getElementById("edit_" + btnID);
                var postEle = document.getElementById("post_" + btnID);

                if(editEle.style.display === "none"){
                    editEle.style.display = "inline-block";
                    postEle.style.display = "none";
                 
                }else{
                    editEle.style.display = "none";
                    postEle.style.display = "inline-block";
                 
                }
            }
        </script>
<?php
        }
    }
?>

<?php
  function securechecklogin($username, $password){
    $mysqli = connect2database();
    $prepared_sql = "SELECT * FROM users WHERE enabled=1 AND username= ? " .
                    " AND password=password(?);";
    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";
    $stmt->bind_param("ss", $username,$password);
    if(!$stmt->execute()) echo "Execute Error";
    if(!$stmt->store_result()) echo "Store_result Error";
    $result = $stmt;

    if ($result->num_rows == 1)
        return TRUE;
    
    return FALSE;
  }
?>

<?php
    function connect2database(){
        $mysqli = new mysqli('localhost',
                  'nguyenmalek' /*database username */,
                  'mysecretP4$$' /* database password */,
                  'secadproject' /* database name*/);
        if($mysqli->connect_errno){
            printf("Database connection failed: %s\n", $mysqli->connect_error);
            exit();
        }
        return $mysqli;
    }

?>

<?php
    function displayedits($btn_id, $message, $post_id, $type){
?>
        <span id = "<?php echo "edit_". htmlentities($btn_id);?>" style="display: none">
            <form action = "edit.php" class="edit_btn" method="POST">
                <textarea rows="1" cols="30" name="message"><?php echo htmlentities($message) ?></textarea>
                <input type="hidden" name="post_id" value="<?php echo htmlentities($post_id) ?>"/>
                <input type="hidden" name="type" value="<?php echo htmlentities($type) ?>"/>
                <button class="button" type="submit">
                Edit 
                </button>
            </form>
        </span>
<?php
    }
?>
