<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Registration page - SecAD</title>
</head>
<body>
      	<h1>A Simple Registration form, SecAD</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="addnewuser.php" method="POST" class="form login"> 
                Username:
                <input type="text" class="text_field" name="username" 
                  pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$"
                  title="Please enter a valid email as username"
                  placeholder="Your email address"
                  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title:'');"/> <br>

                Password: 
                <input type="password" class="text_field" name="password" required
                  placeholder="Enter password"
                  pattern="^(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&])(\w!@#$%^&]{8,}$"
                  title="Password must have at least 8 characters with one uppercase letter, one lowercase letter, one number, and one special character (!@#$%^&)"
                  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"
                  onkeyup="document.forms[0].confirmpassword.pattern = RegExp(this.value);" /> <br>

                <button class="button" type="submit">
                  Sign Up
                </button>
          </form>

</body>
</html>

