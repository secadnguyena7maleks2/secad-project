<?php
	require "session_auth.php";
	require "database.php";
	$username=$_SESSION["username"];
	$message=$_REQUEST["message"];
	$post_id=$_POST["post_id"];
	$type=$_POST["type"];
	
	if (isset($username) AND isset($message) AND isset($post_id) AND isset($type)) {
		if (!edit($post_id, $message, $type)) {
			echo "<script>alert('Error: Post could not be updated.');</script>";
		}
	} else {
        echo "<script>alert('Error: provided no post.');</script>";
		exit();
	}
    header("Refresh:0; url=index.php");
?>
