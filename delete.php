<?php
	require "session_auth.php";
	require "database.php";
	$username=$_SESSION["username"];
	$post_id=$_GET["post_id"];
	$type = $_GET["type"];
	
	if (isset($username) AND isset($post_id) AND isset($type)) {
		if (deleteitem($post_id, $type)) {
			echo "<script>alert('The post/comment has been deleted.');</script>";
		} else {
			echo "<script>alert('Error: Cannot delete the post/comment.');</script>";
		}
	} else {
		echo "<script>alert('No provided post/comment to delete.');</script>";
		exit();
	}
	header("Refresh:0; url=index.php");
?>
