SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS posts;
CREATE TABLE posts(
    posts_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    time_stamp TIMESTAMP,
    message varchar(250),
    owner varchar(50),
    FOREIGN KEY (owner) REFERENCES users(username) ON DELETE CASCADE
);

DROP TABLE IF EXISTS comments;
CREATE TABLE comments(
    posts_id int,
    message varchar(250),
    time_stamp TIMESTAMP,
    owner varchar(50),
    comments_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    FOREIGN KEY (owner) REFERENCES users(username) ON DELETE CASCADE,
    FOREIGN KEY (posts_id) REFERENCES posts(posts_id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS users;
CREATE TABLE users(
    username varchar(50) PRIMARY KEY,
    password varchar(100) NOT NULL,
    enabled int
);

INSERT INTO users (username, password, enabled) VALUES ('nguyenmalek', 'mysecretP4$$', 1);
UPDATE users SET password=password('mysecretP4$$') WHERE username='nguyenmalek';

SET FOREIGN_KEY_CHECKS=1;
